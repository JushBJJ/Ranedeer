[extern printo]
global test_syscall

test_syscall:
mov eax, test
mov ebx, 0x02
mov ecx, 0x0F
push ecx
push ebx
push eax
call printo
pop eax
pop ebx
pop ecx
ret

test: db "[%kAssembly Test%k] Testing printo from assembly!",10,0
; This somewhat makes the background change: arg_1: db 0x02, 0x0F ; Green, White 
