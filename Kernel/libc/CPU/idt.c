#include <idt.h>
#include <stdint.h>

void SetIdtGate(int n,uint32_t handler){
  idt[n].lowoffset=low_16(handler);
  idt[n].sel=KernelCS;
  idt[n].always0=0;
  idt[n].flags=0x8E;
  idt[n].highoffset=high_16(handler);
}

void SetIdt(){
  idtreg.base=(uint32_t)&idt;
  idtreg.limit=IDTENTRIES*sizeof(idtgatet)-1;
  __asm__ __volatile__("lidtl (%0)" : : "r" (&idtreg));
}
