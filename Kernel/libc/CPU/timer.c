#include <timer.h>
#include <isr.h>
#include <ports.h>
#include <Kernel.h>

uint32_t tick = 0;

static void TimerCallback(registersT *regs) {
    tick++;
    unused(regs);
}

void initTimer(uint32_t freq) {
    /* Install the function we just wrote */
    RegisterInterruptHandler(IRQ0, TimerCallback);

    /* Get the PIT value: hardware clock at 1193180 Hz */
    uint32_t divisor = 1193180 / freq;
    uint8_t low  = (uint8_t)(divisor & 0xFF);
    uint8_t high = (uint8_t)( (divisor >> 8) & 0xFF);
    /* Send the command */
    PBO(0x43, 0x36); /* Command port */
    PBO(0x40, low);
    PBO(0x40, high);
}


void Wait(int Sec){
  Sec=Sec*15000000;
  int i;
  while(i<Sec){
    i++;
  }
}
