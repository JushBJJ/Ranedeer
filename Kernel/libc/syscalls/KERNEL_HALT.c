#include <syscalls.h>

int KERNEL_HALT(int EXIT_CODE){
	KERNEL_STATUS = KERNEL_STATUS_HALT;
	asm("hlt");
}
