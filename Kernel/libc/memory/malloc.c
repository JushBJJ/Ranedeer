#include <memory.h>
#include <stdio.h>

extern char Block[100000];
extern int Next_ID;
extern int Block_ID[1000];
extern int Bytes_Left;
extern void *Next_Free_Address;
extern void *Block_Start[1000];
extern void *Block_End[1000];
extern void *Start;
extern void *Last;

void *malloc(int Bytes){
        if(Block_ID[0]==0){
		Bytes_Left=sizeof(Block);
		Next_Free_Address=&Block[1];
		Block_ID[0]=1;
        }

	if(Bytes_Left<Bytes || Bytes_Left==0){
		printf("Unable to allocate bytes.\n");
		return NULL;
	}

	char *ptr=Next_Free_Address;
	Block_Start[Block_ID[Next_ID]]=&ptr[0];
	Block_End[Block_ID[Next_ID]]=&ptr[Bytes++];
	
	Next_Free_Address=Block_End[Block_ID[Next_ID]]++;
	Next_ID++;
	Bytes_Left=Bytes_Left+Bytes;
	return &ptr[0];	
}
