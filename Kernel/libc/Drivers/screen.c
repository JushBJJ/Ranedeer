#include <screen.h>
#include <ports.h>
#include <stdbool.h>
#include <string.h>
#include <Data.h>
#include <stdio.h>
#include <types.h>
#include <stdarg.h>
#include <memory.h>

/* Screen Offset */

// Getting Offsets
  int GetOffset(int col, int row){
    return 2 * (row * _MAXCOL + col);
  }
  int GetOffsetRow(int offset){
    return offset / (2 * _MAXCOL);
  }

  int GetOffsetCol(int offset){
    return (offset - (GetOffsetRow(offset)) * 2 * _MAXCOL) / 2;
  }

  int GetCursorOffset(){
    int ret = 0;

          PBO(_REGSCREENCTRL, 14);
    ret = PBI(_REGSCREENDATA) << 8;
          PBO(_REGSCREENCTRL, 15);
    ret +=PBI(_REGSCREENDATA);

    return ret * 2;
}

// Set the position of the cursor (NOT POINTER)
  void SetCursorPosition(int col, int row){
    CR_PRINTO(" ", col, row); // The space is a dummy char
    Backspace(); // Backspace because the cursor will move after it put's the char

    return;
  }

  void Backspace(){
    
    /* Backspace get's the cursor offset then remove's the character whom is next to the cursor block */
    int offset = GetCursorOffset() - 2;
    int col    = GetOffsetCol(offset);
    int row    = GetOffsetRow(offset);

    putchar(0x08,col,row,_NormalColor); // 0x08 is Backspace (in hexadecimal) the reason why it's not 0x00 because the cursor won't move 
    return;
  }

void SetCursorOffset(int offset){
  offset /= 2;

  PBO(_REGSCREENCTRL, 14);
  PBO(_REGSCREENDATA, (unsigned char)(offset>>8));
  PBO(_REGSCREENCTRL, 15);
  PBO(_REGSCREENDATA, (unsigned char)(offset&0xff));

  return;
}

int putchar(char byte, int col, int row, char attr){
  u8 *_VMADDR = (u8*)_VMAddress; // 
  if(attr == 0x00){
    attr=_NormalColor;
  }

  if(col>=_MAXCOL||row>=_MAXROW){
    _VMADDR[2*(_MAXCOL)*(_MAXROW)-2]='E';
    _VMADDR[2*(_MAXCOL)*(_MAXROW)-1]=_ErrorColor;
    return GetOffset(col,row);
  }

  int offset;
  if(col>=0&&row>=0){
    offset=GetOffset(col,row);
  }
  else{
    offset=GetCursorOffset();
  }
  if(byte=='\n'){
    row=GetOffsetRow(offset);
    offset=GetOffset(0,row+1);
  }
  else if(byte==0x08){
    _VMADDR[offset]=' ';
    _VMADDR[offset+1]=attr;
  }
  else{
    _VMADDR[offset]=byte;
    _VMADDR[offset+1]=attr;
    offset+=2;
  }

  if(offset>=_MAXROW*_MAXCOL*2){
    int i;
    for(i=1;i<_MAXROW;i++){
      memcp((u8*)(GetOffset(0,i)+_VMADDR),
            (u8*)(GetOffset(0,i-1)+_VMADDR),
            _MAXCOL*2);
    }
    char *lastline=(char*)(GetOffset(0,_MAXROW-1)+(u8*)_VMADDR);
    for(i=0;i<_MAXCOL*2;i++){
      lastline[i]=0;
    }
    offset-=2*_MAXCOL;
  }
  SetCursorOffset(offset);
  CC=col;
  CR=row;
  return offset;
}
void clear(){
  int screensize=_MAXROW*_MAXCOL;
  int i;
  char *screen=_VMAddress;

  for(i=0;i<screensize;i++){
    screen[i*2]=' ';
    screen[i*2+1]=_NormalColor;
  }
  SetCursorOffset(GetOffset(0,0));
}
