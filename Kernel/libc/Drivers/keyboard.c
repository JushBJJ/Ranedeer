
#include <keyboard.h>
#include <stdio.h>
#include <screen.h>
#include <Data.h>
#include <ports.h>
#include <__IN__.h>
#include <string.h>

#define KEYBOARD struct Keyboard

extern int Input_Block_Pointer;
extern char Input_Block[1024];
extern char BUFFER[1024];
extern bool KEYBOARD_ON;
extern bool Enter;
extern bool Get_Input;

const char *key_name[] = {"ERROR", "Esc", "1", "2", "3", "4", "5", "6",
                          "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E",
                          "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl",
                          "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`",
                          "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".",
                          "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char key_ascii[] = {'?', '?', '1', '2', '3', '4', '5', '6',
                          '7', '8', '9', '0', '-', '=', '?', '?', 'Q', 'W', 'E', 'R', 'T', 'Y',
                          'U', 'I', 'O', 'P', '[', ']', '?', '?', 'A', 'S', 'D', 'F', 'G',
                          'H', 'J', 'K', 'L', ';', '\'', '`', '?', '\\', 'Z', 'X', 'C', 'V',
                          'B', 'N', 'M', ',', '.', '/', '?', '?', '?', ' '};

void Reset_Input(){
    for(int i=0;i<sizeof(Input_Block);i++)
        Input_Block[i]=0x00;
    return;
}

void Key_On(char *TYPE, KEYBOARD *Keyboard_Info)
{
    Keyboard_Info->L_SHIFT = false;
    Keyboard_Info->L_ALT = false;
    Keyboard_Info->SPACE = false;
    Keyboard_Info->R_SHIFT = false;
    Keyboard_Info->L_CTRL = false;
    Keyboard_Info->BACKSPACE = false;
    Keyboard_Info->ENTER = false;
    Keyboard_Info->ESC = false;
    Keyboard_Info->KEYPAD = false;
    Keyboard_Info->NONE = false;
    Keyboard_Info->TAB = false;

    if (!strcmp(TYPE, "L_SHIFT"))
        Keyboard_Info->L_SHIFT = true;
    if (!strcmp(TYPE, "L_ALT"))
        Keyboard_Info->L_ALT = true;
    if (!strcmp(TYPE, "SPACE"))
        Keyboard_Info->SPACE = true;
    if (!strcmp(TYPE, "R_SHIFT"))
        Keyboard_Info->R_SHIFT = true;
    if (!strcmp(TYPE, "L_CTRL"))
        Keyboard_Info->L_CTRL = true;
    if (!strcmp(TYPE, "BACKSPACE"))
        Keyboard_Info->BACKSPACE = true;
    if (!strcmp(TYPE, "ENTER"))
        Keyboard_Info->ENTER = true;
    if (!strcmp(TYPE, "ESC"))
        Keyboard_Info->ESC = true;
    if (!strcmp(TYPE, "KEYPAD"))
        Keyboard_Info->KEYPAD = true;
    if (!strcmp(TYPE, "NONE"))
        Keyboard_Info->NONE = true;
    if (!strcmp(TYPE, "TAB"))
        Keyboard_Info->TAB = true;
}

void Append_Buffer(char c)
{
    if (Input_Block_Pointer != sizeof(Input_Block))
    {
        Input_Block[Input_Block_Pointer] = c;
        Input_Block_Pointer++;
        Input_Block[Input_Block_Pointer] = '\0';
    }
    return;
}

void Backspace_Buffer()
{
    if (Input_Block_Pointer != 0)
    {
        Input_Block[Input_Block_Pointer] = 0x00;
        Input_Block_Pointer--;
    }
    return;
}

static void KeyboardCallback(registersT regs)
{
    KEYBOARD *k;
    u8 scancode = PBI(0x60);
    if (scancode > 57)
        return;

    if(Get_Input==false)
        k->SHOW_KEY=false;
    else
        k->SHOW_KEY=true;

    // Backspace
    if (scancode == 0x0E)
    {
        Backspace_Buffer();
        if (k->SHOW_KEY == true)
            Backspace();
    }
    else if (scancode == 0x1C) // Enter
    {
        Append_Buffer('\n');
        Enter=true;
        if (k->SHOW_KEY == true)
            printf("\n");
    }
    else
    {
        char letter = key_ascii[(int)scancode];
        char str[2] = {letter, '\0'};
        Append_Buffer(letter);
        if (k->SHOW_KEY == true)
        {
            printf("%c", letter);
        }
    }

    unused(regs);
}

void initKeyboard()
{
    RegisterInterruptHandler(IRQ1, KeyboardCallback);
}
