#include <stdio.h>
#include <Data.h>
#include <color.h>
#include <memory.h>
#include <stdbool.h>
#include <types.h>
#include <screen.h>
#include <keyboard.h>
#include <string.h>

void printc(char c)
{
    int Cursor_Offset_Column = GetOffsetCol(GetCursorOffset()); // Column number
    int Cursor_Offset_Row = GetOffsetRow(GetCursorOffset()); // Row number

    putchar(c, Cursor_Offset_Column, Cursor_Offset_Row, Current_Color); // Output a char
    return;
}

void printnum(int num)
{
    printo("%s", itoa(num)); // Output number
    return;
}

int CR_PRINTO(char *Message, int col, int row)
{
    // CR_PRINTO(Char *Message, int col, int row) put's Message on the screen on a certain Column and Row

    /* Variables */
    int offset;
    int i = 0;

    offset = GetOffset(col, row); // Set the offset where the cursor is going to be in

    /* While the Message isn't 0x00, put each char in the variable then increment the integer 'i' */
    for(; Message[i] != 0x00; i++)
    {
        printc(Message[i]);
    }
    return GetCursorOffset(); // Return offset
}

void printo(const char *message, ...)
{
    va_list ap;            // Arg list
    va_start(ap, message); // Start the argument collecting (lol)

    vprinto(message, ap); // Main function of printo

    va_end(ap); // End the crap already
}

void vprinto(const char *message, va_list ap)
{
    /* Variables */
    int pointer = 0;
    char *str;
    int decimal = 0;
    char c;
    bool Memory_Was_Allocated = false;

    /* Loop until end of the shit */
    while (message[pointer] != '\0')
    {
	/* If message[pointer] has '%' then go through the process */
        if (message[pointer] == '%')
        {
	    /* [x][%][s][B][C]
	     *	      ^Pointer must be updated to reach this
	     */
            
	    pointer++;

            switch (message[pointer])
            {
	    /* replaced and will print out a string */
            case 's': // Used for Strings
                pointer++; // Get out of '%s'
                str = va_arg(ap, char *); // Get the next arg and return it as a (char *)
                if (str[0] != 0x00) // Just in case str[0] == 0x00
                {
                    printo(str); // Print out the string (arg)
                }

		// Else, break
                break;
            case 'd': // Used for integers

		pointer++; // Update
       
       		decimal = va_arg(ap, int); // Return as int
                printnum(decimal); // Print as a decimal (but translated as a string)

                break;
            case 'c': // Print a singular char

                pointer++; // Update

                c = va_arg(ap, int); // Take it as an integer, char's =  int ?? idk ask the compiler
                printc(c); // Print one char

                break;
            case 'k': // Used to change the color
                pointer++; //Update
                Current_Color = va_arg(ap, int);  // Change the Current_Color variable
		break;
            case 'b': // Used for print in "True" or "False" whether if they're true or false
                pointer++; // Update
                decimal = va_arg(ap, int); // True = 1, False = 0
	
		/************************/	
		/* Check */
		switch(decimal){
			case true:
				printo("TRUE");
				break;
			case false:
				printo("FALSE");
				break;
			default:
				printo("UNKNOWN");
				break;
		}	
		/*************************/
                break;
            default: // If that's not any case then output normally
                pointer = pointer - 1; // (Will print out '%')
                printc(message[pointer]); 
		pointer++; // Update
                break;
            }
        }
        else
        {
           	printc(message[pointer]); 
		pointer++;
        }
    }
}
