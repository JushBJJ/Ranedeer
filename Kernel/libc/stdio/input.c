#include <__IN__.h>
#include <isr.h>
#include <keyboard.h>
#include <stdio.h>
#include <string.h>
#include <ports.h>

#define BACKSPACE 0x0E
#define ENTER 0x1C

const char *key_name[] = {"ERROR", "Esc", "1", "2", "3", "4", "5", "6",
						  "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E",
						  "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl",
						  "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`",
						  "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".",
						  "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char key_ascii[] = {'?', '?', '1', '2', '3', '4', '5', '6',
						  '7', '8', '9', '0', '-', '=', '?', '?', 'Q', 'W', 'E', 'R', 'T', 'Y',
						  'U', 'I', 'O', 'P', '[', ']', '?', '?', 'A', 'S', 'D', 'F', 'G',
						  'H', 'J', 'K', 'L', ';', '\'', '`', '?', '\\', 'Z', 'X', 'C', 'V',
						  'B', 'N', 'M', ',', '.', '/', '?', '?', '?', ' '};

extern char Input_Block[1024];
extern int Input_Block_Pointer;
extern bool Enter;
extern bool Get_Input;

char *input(int buf)
{
	Get_Input = true;
	while (Enter != true)
	{
		printf("");
	}
	Get_Input = false;

	// Cut down
	int i = 0;
	for (; i < buf && i < sizeof(Input_Block); i++)
	{
	}

	while (i < sizeof(Input_Block))
	{
		Input_Block[i] = 0x00;
		i++;
	}

	return Input_Block;
}
