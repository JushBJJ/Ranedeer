#include <string.h>
#include <stdio.h>
#include <isr.h>
#include <string.h>
#include <color.h>
#include <timer.h>
#include <keyboard.h>
#define printk printo
#define debug printo

extern bool Get_Input;
extern char Input_Block[1024];

extern int test_syscall(void);

int test(void){
	printf("test\b");
	return 0;
}

void startkernel()
{
	clear();
	printo("%kKernel Loading\n\n", White_Text);

	printo("Testing Text Color: \n");
	for (int i = 0; i < (sizeof(Colors) / sizeof(Colors[0])); i++)
	{
		printo("Color[%d]: %kX%k\n", i, Colors[i], White_Text);
	}

	printf("\n\n");

	/* Interrupt Initialization */
	isr_install(); //Install interrupts
	printo("[%kKERNEL%k] Installed Interrupts\n", Green_Text, White_Text);
	initTimer(50);  // Initialize Timer
	initKeyboard(); // Initialize Keyboard
	Get_Input=false;
	printo("[%kKERNEL%k] Initialized Timer and Keyboard\n", Green_Text, White_Text);
	EnableInterrupts(); // Enable Interrupts

	printo("[%kKERNEL%k] Enabled Interrupts\n\n", Green_Text, White_Text);

	test_syscall();
	printf("\n> ");

	char *in=(char *)malloc(100);
	strcpy(in, input(100));
	char *Pre=(char *)malloc(100);
	char *Post=(char *)malloc(100);
	char *Org=(char *)malloc(100);
	strcpy(Org, in);	

#define Before 0
#define After 1

	// This code can be used as an example
	if(strcontain(in, ' ')){
		strcpy(Pre, strtok(in, ' ', Before));
		strcpy(Post, strtok(in, ' ', After));

		printf("Pre: %s\nSub: %s\nOriginal: %s", Pre, Post, Org);
	}
	else{
		printf("false\n");
	}

	return;
}
