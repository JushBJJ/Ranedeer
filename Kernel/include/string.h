#ifndef STRING_H
  #define STRING_H
  #include <types.h>
  #include <stdbool.h>
  static int ptr;
  static int pointer = 0; //Pointer used for OG_String, it will get reset back to 0 when OLD_NEW is equal to 1
  static char *OG_String; //Static just in case, but will get reset when OLD_NEW is equal to 1

  int strlen(char *str);
  void IntToAscii(int num, char target[]);
  int strcmp(const char* s1,const char *s2);

  char *strtok(char *str, char token, int Before_After);
  
  void m_strcpy(char *x, char *y);
  char *strncpy(char *Origin, char *Copied, size_t SizeOfString);
  char *strcpy(char *Origin,char *Copied);
  str_t strstr(char* str,char* sub);
  char *itoa(int val);
  int strcspn(char *text,char target);
  int strcontain(char *String,  char Target);
#endif
