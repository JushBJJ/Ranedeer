#ifndef MEMORY_HEADER
#define MEMORY_HEADER

#include <types.h>
#include <stdint.h>

/* Malloc */
char Block[100000]; //100000 bytes = 100KB

// Memory Block Information
int Next_ID;
int Block_ID[1000];
int Bytes_Left;
void *Next_Free_Address;
void *Block_Start[1000];
void *Block_End[1000];
void *Start=&Block[0];
void *Last=&Block[100000];

/* Functions */

void* memcpy(void *dest, void *src,size_t n);
void* memset(void *dst,int c,size_t n);
void initialize();

void *malloc(int Bytes);
void malloc2(char *a,size_t size);

/* MEMCP IS OLD AND ONLY USED FOR screen.c */
void memcp(u8 *source,u8 *destination,int bytes);
#endif
