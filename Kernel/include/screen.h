#ifndef SCREEN_H
  #define SCREEN_H

  #define _VMAddress (char *)0xb8000 // Address of video memory

  #define _MAXROW 25
  #define _MAXCOL 80

  #define _NormalColor   0x0f // Wnite
  #define _ErrorColor    0xf4 //Red

  #define _REGSCREENCTRL 0x3D4
  #define _REGSCREENDATA 0x3D5

  #ifndef NULL
    #define NULL ((void *)0)
  #endif

  /* These define's may not ever been used but it's there just in case */
  #define printf printo
  #define Clear_Screen clear
  #define Set_Cursor_Positon SetCursorPosition
  #define Do_Backspace

  //Functions

  /* Offset-Related */
  void  SetCursorPosition(int col, int row);
  void  SetCursorOffset(int offset);
  int   GetCursorOffset();
  int   GetOffset(int col, int row);
  int   GetOffsetRow(int offset);
  int   GetOffsetCol(int offset);

  /* Modifying the Screen text (Not cursor ) */
  void clear();      // Clears the Screen
  void Backspace(); // Clear's 

#endif
