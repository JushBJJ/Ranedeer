#ifndef __IN__H
  #define __IN__H
  #include <stdbool.h>

  char Input_Block[1024]; // 1 KB
  void SetCursorBarrier(int offset);
  void SetCursorBarrierHere();
  void DisableCursorBarrier();
#endif
