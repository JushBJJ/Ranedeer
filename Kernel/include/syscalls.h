#ifndef SYSCALLS_H
#define SYSCALLS_H

#include <stdio.h>
#include <screen.h>
#include <keyboard.h>
#include <Kernel.h> // Syscall variables are going to be stored in there

/* Syscall 1 */
int KERNEL_HALT(int EXIT_CODE);

/* Syscall 2 */
int KERNEL_PRINT_CHAR(int CHAR_HEX);

#endif
