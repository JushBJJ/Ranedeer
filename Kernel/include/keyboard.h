#ifndef KEYBOARD_H
#define KEYBOARD_H
#include <types.h>
#include <memory.h>
#include <__IN__.h>
#include <stdbool.h>
int Input_Block_Pointer;
char BUFFER[1024];
bool Enter;
bool Get_Input;

struct Keyboard
{
  u8 key_code;

  bool SHOW_KEY;

  /* shift, alt, etc */
  bool L_SHIFT;
  bool L_ALT;
  bool SPACE;
  bool R_SHIFT;
  bool L_CTRL;
  bool BACKSPACE;
  bool ENTER;
  bool ESC;
  bool KEYPAD;
  bool TAB;
  bool NONE;
};

void Reset_Input();
void initKeyboard();
void BackspaceChar();
void AddtoChar(char c);
void SendInput();
#endif
